"strict-mode";
import { User, Toilet, Review, ReviewVotes } from "../../types";
import { verifyToken } from "./token";
import * as jwt from 'jsonwebtoken';
import db from "./db-service";
import express, { response } from "express";

const dbRouter = express.Router()

/**
 * !!!!!!!!!!!!!!!!!!
 * Toilet requests
 * !!!!!!!!!!!!!!!!!!
 */

/**
 * Get "/api/v1/toilets"
 * Gets all toilets from the database
 * 
 * request body: null
 * @returns Toilet[]
 */
dbRouter.get("/toilets", (request, response) => {
    //All toilets.
    db.getAllToilets()
        .then((res) => {
            response.status(200);
            response.json(res);
        })
        .catch((err) => {
            response.status(500);
            response.send("Database unreachable");
        });
});

/**
 * Get "api/v1/toilets/:id"
 * Gets a toilet, defined by id in the url
 * 
 * request body: null
 * @returns Toilet
 */
dbRouter.get("/toilets/:id", (request, response) => {
    //Get toilet with id
    const id: number = Number(request.params.id);

    db.getToilet(id)
        .then((res) => {
            if (res.length < 1) {
                response.status(404);
                response.send(`Cannot find toilet with id ${id}`);
            } else {
                response.status(200);
                response.json(res[0]);
            }
        })
        .catch((err) => {
            response.status(500);
            response.send("Database unreachable");
        });
});

/**
 * GET "/api/v1/toilet/:id/average"
 * Gets out the toilet's average rating, from  reviews
 * 
 * @param toiletid
 */
dbRouter.get("/toilets/:id/average", (request, response) => {
    const id: number = Number(request.params.id);

    db.getToiletAverage(id)
        .then((res) => {
            if (res.length < 1 ) {
                response.status(404);
                response.send(`Cannot find average from toilet with id ${id}`)
            } else {
                response.status(200);
                response.send(res);
            }
        })
        .catch((err) => {
            response.status(500);
            response.send("Database error! " + err);
        })
})

/**
 * Post "api/v1/toilets"
 * For adding a new toilet, defined by it's name and room (poi id)
 * 
 * request body consists of [name, poi_id, campus_id] and any other of these params:
 * @param name: string  used to identify the toilet by name
 * @param description*: string  a description of the toilet
 * @param gender*: number  0 = both, 1 = man, 2 = woman, 3 = handicap, 4 = unknown
 * @param facilities*: string  a description of the facilites at the toilet
 * @param poi_id: number  the id of the poi, from mazemap
 * @param campus_id: number  the id of the campus, from mazemap
 * @param image_link*: string  a link to an image
 * @returns void
 */
dbRouter.post("/toilets", (request, response) => {
    const toilet: Toilet = request.body;

    // is it possible to not have type undefined here?
    let token: string | undefined = request.headers.authorization;
    let decodedToken: jwt.JwtPayload;
    if (!(toilet && toilet.name !== "" && toilet.poi_id !== undefined && toilet.campus_id !== undefined)) {
        response.status(400);
        response.send("A toilet needs the properties: name, campus_id and poi_id");
        return
    }
    if (!token) {
        token = "returning false";  // sets token to avoid typescript errors further down :rolling_eyes:
        response.status(401)
        response.send("Unauthorized, no token specified!")
        return
    };

        verifyToken(token)
        .then((e) => {
            decodedToken = e;
    
            db.createToilet(toilet, decodedToken)
            .then(() => {
                response.status(201)
                response.send("YEAH! succesfully created a toilet");
                return
            })
            .catch((err) => {
                response.status(500)
                response.send("Database error! " + err)
                return
            })
    }).catch((err) => {
        response.status(500);
        response.send("Problem with verifying token");
    })
});

/**
 * Put "api/v1/toilets/:id"
 * Edits an existing toilet, defined by it's id in the database, gotten by the url
 * 
 * request body: {Toilet, User}
 * @returns Toilet
 */
dbRouter.put("/toilets/:id", (request, response) => {

    const toilet: Toilet = request.body;

    let token: string | undefined = request.headers.authorization;
    let decodedToken: jwt.JwtPayload;

    if (!(toilet && toilet.name !== "" && toilet.poi_id !== undefined && toilet.campus_id !== undefined)) {
        response.status(400);
        response.send("A toilet needs the properties: name, campus_id and poi_id");
    }

    if (!token) {
        token = "returning false";  // sets token to avoid typescript errors further down :rolling_eyes:
        response.status(401)
        response.send("Unauthorized, no token specified!")
    } else {
            verifyToken(token)
            .then((e) => {
                    decodedToken = e;
                    db.editToilet(toilet, decodedToken)
                    .then(() => {
                        response.status(200);
                        response.send("YEAH! succesfully updated a toilet");
                    })
                    .catch((err) => {
                        response.status(500)
                        response.send("Database error! " + err)
                    })
                
            })
            .catch((e) => {
                response.status(500)
                response.send("Problem with verifying token")
            })
    }
});

/**
 * DELETE "api/v1/toilets/:id"
 * Deletes an existing toilet, and it's connected reviews?
 * 
 * request headers:
 *    Authorization: {token}  // only admins and the creator of the toilet can delete it
 * 
 */
dbRouter.delete("/toilets/:id", (request, response) => {
    const id: number = Number(request.params.id);
    let token: string | undefined = request.headers.authorization;
    let decodedToken: jwt.JwtPayload;

    if (!token) {
        token = "returning false";  // sets token to avoid typescript errors further down :rolling_eyes:
        response.status(401);
        response.send("Unauthorized, no token specified!");
    } else {
        verifyToken(token)
            .then((e) => {
            decodedToken = e;
    
            db.deleteToilet(id, decodedToken)
                .then(() => {
                    response.status(200);
                    response.send("YEAH! success at trying to delete toilet with id: " + id);
                })
                .catch((err) => {
                    response.status(500);
                    response.send("Database error! " + err);
                })
    
            })
            .catch((e) => {
                response.status(500);
                response.send("Problem with verifying token");
            })
    }
});

/**
 * !!!!!!!!!!!!!!!!!!
 * Review requests
 * !!!!!!!!!!!!!!!!!!
 */

/**
 * GET "/api/v1/reviews"
 * Gets all existing reviews
 * 
 * request body: null
 * @returns Review[]
*/
dbRouter.get("/reviews", (request, response) => {
    //Hente alle reviews
    db.getAllReviews()
        .then((res) => {
            response.status(200);
            response.json(res);
        })
        .catch((err) => {
            response.status(500);
            response.send("Database unreachable! " + err);
        });
});

/**
 * GET "/api/v1/reviews/toilet/:toiletId" 
 * Gets all reviews for a toilet
 * 
 * request body: null
 * @returns Reviews[]
 */
dbRouter.get("/reviews/toilet/:toiletId", (request, response) => {
    //Reviews tilhørende do med en gitt id
    const id: number = Number(request.params.toiletId);

    db.getToiletReviews(id)
        .then((res) => {
            if (res.length < 1) {
                response.status(404);
                response.send(`Cannot find reviews for toilet with id ${id}`);
            } else {
                response.status(200);
                response.json(res);
            }
        })
        .catch((err) => {
            response.status(500);
            response.send("Database unreachable! " + err);
        });
});

/**
 * GET "/api/v1/reviews/:reviewId" 
 * Gets a single review
 * 
 * request body: null
 * @returns Review[] Where review[0] is the correct one
 */
dbRouter.get("/reviews/:reviewId", (request, response) => {
    //Henter et review med en gitt id
    const id: number = Number(request.params.reviewId);

    db.getReviewFromId(id)
        .then((res) => {
            
            if (res.length < 1) {
                response.status(404);
                response.send(`Cannot find reviews for toilet with id ${id}`);
            } else {
                response.status(200);
                response.json(res);
            }
        })
        .catch((err) => {
            response.status(500);
            response.send("Database unreachable! " + err);
        });
});

/**
 * GET "/api/v1/reviews/user/:userId" 
 * Gets all reviews from a user
 * 
 * request body: null
 * @returns Review[]
 */
dbRouter.get("/reviews/user/:userId", (request, response) => {
    const id: number = Number(request.params.userId);

    db.getUserReviews(id)
        .then((res) => {
        if (res.length < 1) {
            response.status(404);
            response.send(`Cannot find reviews for user with id ${id}`);
        } else {
            response.status(200);
            response.json(res);
        }
        })
        .catch(err => {
            response.status(500);
            response.send("Database unreachable! " + err);
        })
  
})

/**
 * GET "/api/v1/reviews/:reviewId/votes"
 * Gets all votes for a review
 * 
 * request body: null
 * @returns object[]  where object[0] is the sum
 */
dbRouter.get("/reviews/:reviewId/votes", (request, response) => {
    const id: number = Number(request.params.reviewId);

    db.getReviewRating(id)
        .then((res) => {
            if (res.length < 1) {
                response.status(404);
                response.send(`Cannot find votes for review with id ${id}`);
            } else {
                response.status(200);
                response.json(res);
            }

        })
        .catch((err) => {
            response.status(500);
            response.send("DATABASE ERROR! " + err)
        })

})

/**
 * POST "/api/v1/reviews"
 * Creates a new review
 * 
 * @param {Review}
 * @returns ??
 */
dbRouter.post("/reviews", (request, response) => {
    const review: Review = request.body;
    let token: string | undefined = request.headers.authorization;
    let decodedToken: jwt.JwtPayload;

    if (!token) {
        token = "send it away johnny!"
        response.status(401);
        response.send("Unauthorized, no token specified!")
    } else {
        if (review.hasOwnProperty("toilet_id")) 
        {
            verifyToken(token)
            .then((e) => {
                decodedToken = e;
            
                db.createReview(review, decodedToken)
                    .then(() => {
                        response.status(201);
                        response.send(`Success! Created a new review for toilet with id ` + review.toilet_id);
                    })
                    .catch(err => {
                        response.status(500);
                        response.send("Database unreachable! " + err);
                    })
                    
                    
                })
                .catch((e) => {
                    response.status(500);
                    response.send("Problem with verifying token");
                })
        } else {
            response.status(400);
            response.send("A review needs the property \"toilet_id\"");
        }
    }
});

/**
 * PUT "/api/v1/reviews/:reviewid"
 * Change a review
 * 
 * @param Review
 * @returns ??
 */
dbRouter.put("/reviews/:reviewId", (request, response) => {

    let id = Number(request.params.reviewId);
    const review: Review = request.body;

    // is it possible to not have type undefined here?
    let token: string | undefined = request.headers.authorization;
    let decodedToken: jwt.JwtPayload;

    if (!token) {
        token = "returning false";  // sets token to avoid typescript errors further down :rolling_eyes:
        response.status(401);
        response.send("Unauthorized, no token specified!");
    } else {
        if(
            review.hasOwnProperty('toilet_id')
        ){
            // the request body might have the wrong id, if the user sends their own http call
            review.id = id;
            verifyToken(token)
            .then((e) => {
                decodedToken = e;
                
                db.editReview(review, decodedToken)
                    .then(() => {
                        response.status(200);
                        response.send(`Updated review with id ${id} successfully!`);
                    })
                    .catch(err => {
                        response.status(500);
                        response.send("Database unreachable! " + err);
                    })
                
            })
            .catch((e) => {
                response.status(500)
                response.send("Problem with verifying token")
            })
        }
        else{
            response.status(400);
            response.send('To edit a review, the request body needs at toilet_id')
        }
    }
});

/**
 * DELETE "api/v1/reviews/:id"
 * Deletes an existing review
 * 
 * request header: authtoken
 */
dbRouter.delete("/reviews/:id", (request, response) => {

    const id: number = Number(request.params.id);
    let token: string | undefined = request.headers.authorization;
    let decodedToken: jwt.JwtPayload;

    if (!token) {
        token = "returning false";  // sets token to avoid typescript errors further down :rolling_eyes:
        response.status(401);
        response.send("Unauthorized, no token specified!");
    } else {
        verifyToken(token)
            .then((e) => {
                decodedToken = e;
    
                db.deleteReview(id, decodedToken)
                    .then(() => {
                        response.status(200);
                        response.send("YEAH! success at trying to delete review with id: " + id);
                    })
                    .catch((err) => {
                        response.status(500);
                        response.send("Database error! " + err);
                    })
    
                })
            .catch((e) => {
                response.status(500);
                response.send("Problem with verifying token");
            })
    }
});

/**
 * !!!!!!!!!!!!!!!!!!
 * ReviewVote requests
 * !!!!!!!!!!!!!!!!!!
 */

/**
 * GET "/api/v1/reviewVotes/:reviewId"
 * Gets out all votes for all reviews
 */
dbRouter.get("/reviewVotes", (request, response) => {
    db.getAllReviewVotes()
        .then((res) => {
            response.status(200);
            response.send(res)
        })
        .catch((err) => {
            response.status(500);
            response.send("Database error! " + err);
        })
})

/**
 * GET "/api/v1/reviewVotes/:reviewId"
 * Gets out all votes for a given review, with userid
 * 
 * @param reviewId
 */
dbRouter.get("/reviewVotes/:reviewId", (request, response) => {
    const id: number = Number(request.params.reviewId);

    db.getReviewVotesFromId(id)
        .then((res) => {
            if (res.length < 1) {
                response.status(404);
                response.send(res);
            } else {
                response.status(200);
                response.send(res);
            }
        })
        .catch((err) => {
            response.status(500);
            response.send("Database error! " + err)
        })
})

/**
 * POST "api/v1/reviewVotes/:reviewId/up"
 * Upvotes a review defined by it's id
 * 
 * @param reviewId
 * request header: authentication
 */
dbRouter.post("/reviewVotes/:reviewId/up", (request, response) => {
    const id: number = Number(request.params.reviewId);
    let token: string | undefined = request.headers.authorization;
    let decodedToken: jwt.JwtPayload | undefined;

    if (!token) {
        token = "returning false";  // sets token to avoid typescript errors further down :rolling_eyes:
        response.status(401);
        response.send("Unauthorized, no token specified!");
    } else {
        verifyToken(token)
            .then((e) => {
                decodedToken = e;
    
                // @ts-ignore decodedtoken er ikke undefined, men den kan være "rejected promise"??
                db.voteOnReview(id, 1, decodedToken)
                    .then(() => {
                        response.status(200);
                        response.send("YEAH! upvoted review with id: " + id);
                    })
                    .catch((err) => {
                        if (err.code == "ERR_DUP_ENTRY"){
                            response.status(403);
                            response.send("Duplicate entry! You cannot vote on the same review more than once.");
                        } else {
                            response.status(500);
                            response.send("Database error! " + err);
                        }
                    })
    
                })
            .catch((e) => {
                response.status(500);
                response.send("Problem with verifying token");
            })
    }
})

/**
 * POST "api/v1/reviewVotes/:reviewId/down"
 * Downvotes a review, defined by id
 * 
 * @param reviewId
 * request header: authentication
 */
dbRouter.post("/reviewVotes/:reviewId/down", (request, response) => {
    const id: number = Number(request.params.reviewId);
    let token: string | undefined = request.headers.authorization;
    let decodedToken: jwt.JwtPayload;

    if (!token) {
        token = "returning false";  // sets token to avoid typescript errors further down :rolling_eyes:
        response.status(401);
        response.send("Unauthorized, no token specified!");
    } else {
        verifyToken(token)
            .then((e) => {
                decodedToken = e;
    
                db.voteOnReview(id, -1, decodedToken)
                    .then(() => {
                        response.status(200);
                        response.send("YEAH! downvoted review with id: " + id);
                    })
                    .catch((err) => {
                        if (err.code == "ERR_DUP_ENTRY"){
                            response.status(403);
                            response.send("Duplicate entry! You cannot vote on the same review more than once.");
                        } else {
                            response.status(500);
                            response.send("Database error! " + err);
                        }
                    })
    
                })
            .catch((e) => {
                response.status(500);
                response.send("Problem with verifying token");
            })
    }
})

/**
 * PUT "api/v1/reviewVotes/:reviewId"
 * Removes your vote from the review
 * 
 * @param reviewId
 * request header: authentication
 */
dbRouter.put("/reviewVotes/:reviewId", (request, response) => {
    const id: number = Number(request.params.reviewId);
    let token: string | undefined = request.headers.authorization;
    let decodedToken: jwt.JwtPayload;

    if (!token) {
        token = "returning false";  // sets token to avoid typescript errors further down :rolling_eyes:
        response.status(401);
        response.send("Unauthorized, no token specified!");
    } else {
        verifyToken(token)
            .then((e) => {
                decodedToken = e;
    
                db.removeVote(id, decodedToken)
                    .then((res) => {
                        if (res == "success"){
                            response.status(200);
                            response.send("YEAH! deleted vote from review with id: " + id);
                        } else {
                            response.status(404);
                            response.send("Review not found!")
                        }
                    })
                    .catch((err) => {
                        response.status(500);
                        response.send("Database error! " + err);
                    })
    
                })
            .catch((e) => {
                response.status(500);
                response.send("Problem with verifying token");
            })
    }
})

/**
 * !!!!!!!!!!!!!!!!!!
 * Login Requests
 * !!!!!!!!!!!!!!!!!!
 */

/**
 * POST "/api/v1/login"
 * Logs in an existing user
 * 
 * request body: {username: string, password: string}
 * returns: jwtToken
 */
dbRouter.post("/login", (request, response) => {

    const header = request.headers
    const user: User = request.body;
    if (
        user.hasOwnProperty("username") &&
        user.hasOwnProperty("password")
    ){
        db.login(user)
            .then((res) => {
                response.status(200);
                response.json(res);
                })
            .catch((err) => {
                if (err === "Username or password does not match any known users in the database") {
                    response.status(401);
                    response.json(err)
                } else {
                    // this does catch the bcrypt error, but I don't know what to call that
                    response.status(500);
                    response.json("Database unreachable! " + err);
                }
            });
    } else {
        response.status(400);
        response.send("A user needs a username and password to log in.");
    }
});


/**
 * POST "/api/v1/signup"
 * Creates a new user
 * 
 * Request body: User
 * @returns "Success"
 */
dbRouter.post("/signup/", (request, response) => {
    //Get toilet with id
    const user: User = request.body;
    if (
        user.hasOwnProperty("username") &&
        user.hasOwnProperty("password")
    ){
        db.createUser(user)
            .then((res) => {
                if(res == "duplicate") {
                    response.status(403);
                    response.json("duplicate user")
                } else {
                    response.status(201);
                    response.send(`success! user: ${user.username} is now created!`)  
                }
            })
            .catch((err) => {
                response.status(500);
                response.send("Database unreachable! " + err);
            });

    } else {
        response.status(400);
        response.send("A user needs the properties username and password to be made")
    }
});

/**
 * GET "/api/v1/user"
 * Get user_id from cookie
 * 
 */
dbRouter.get("/user", (request, response) => {
    let token: string | undefined = request.headers.authorization;
    let decodedToken: jwt.JwtPayload;
    if (!token) {
        token = "returning false";  // sets token to avoid typescript errors further down :rolling_eyes:
        response.status(401);
        response.send("Unauthorized, no token specified!");
    }else{
        verifyToken(token).then(e => {
            decodedToken = e;
            response.status(200);
            response.json({user_id: decodedToken.id});
        })
        .catch(() => {
            response.status(500);
            response.send("Problem with verifying token");
        })
    }
})

/**
 * Router object for use with express.router
 */
export default dbRouter
