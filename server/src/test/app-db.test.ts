import { Rating, Review, Toilet, User } from '../../../types';
import { verifyToken } from '../token';
import { pool } from '../mysqlpool';
import * as data from './testdata';
import dbRouter from "../dbRouter";
import db from "../db-service";
import express from "express";
import axios from "axios";
import * as jwt from 'jsonwebtoken';
import {makeHash} from "../hashing"

/**
 * Inside the BeforeEach function there are race conditions when creating resources in the database.
 * Even when the .then()s are nested, the race conditions persist (and made the code less readable)
 * This could be fixed by adding a mutex in the code, or using transactions in the queries, which our mysql service does not support
*/

axios.defaults.adapter = require("axios/lib/adapters/http");
axios.defaults.baseURL = "http://localhost:3003";

let verifiedAdmin: jwt.JwtPayload | User;
let webServer: any;

beforeAll(async () => {
    const app = express();
    app.use(express.json());
    app.use('/api/v1', dbRouter);
    webServer = app.listen(3003);

    await verifyToken(data.adminuser)
        .then(e => {
            verifiedAdmin = e
        })
        .catch(e => {
            throw new Error(e)
        })
});
afterAll((done) => {
    pool.query('TRUNCATE TABLE Toilet', (error) => {
        if (error) return done.fail(error);
        pool.query('TRUNCATE TABLE Review', (error) => {
            if (error) return done.fail(error);
            pool.query('TRUNCATE TABLE ReviewVotes', (error) => {
                if (error) return done.fail(error);
                pool.query('TRUNCATE TABLE `User`', (error) => {
                    if (error) return done.fail(error);
                    pool.end((error) => {
                        if (error) return done.fail(error);
                        done()
                    })
                })
            })
        })
    })
    webServer.close()
});

beforeEach((done) => {

    // Delete all tasks, and reset id auto-increment start value
    pool.query('TRUNCATE TABLE Toilet', (error) => {
      if (error) return done.fail(error);
  
      db
        .createToilet(data.testToilets[0], verifiedAdmin)
        .then(() => db.createToilet(data.testToilets[1], verifiedAdmin))
        .then(() => db.createToilet(data.testToilets[2], verifiedAdmin))  // This leads to a race condition, but even if I do .then(() => {db.create.then(() => {db.create.then(() => {})})}) I get a race condition...
        .then(() => {
            pool.query('TRUNCATE TABLE Review', (error) => {
                if (error) return done.fail(error);

                db
                    .createReview(data.testReviews[0], verifiedAdmin)
                    .then(() => db.createReview(data.testReviews[1], verifiedAdmin))
                    .then(() => db.createReview(data.testReviews[2], verifiedAdmin))
                    .then(() => {
                        pool.query('TRUNCATE TABLE `User`', (error) => {

                            db
                                .createUser(data.testUsers[0])
                                .then(() => db.createUser(data.testUsers[1]))
                                .then(() => db.createUser(data.testUsers[2]))
                                .then(() => {
                                    pool.query('TRUNCATE TABLE ReviewVotes', (error) => {
                                        if (error) return done.fail(error);
            
                                        db
                                            .voteOnReview(1, 1, verifiedAdmin)
                                            .then(() => db.voteOnReview(2, 1, verifiedAdmin))
                                            .then(() => db.voteOnReview(3, 1, verifiedAdmin))
                                            .then(() => db.voteOnReview(4, -1, verifiedAdmin))
                                            .then(() => db.voteOnReview(5, 1, verifiedAdmin))
                                            .then(() => done())
        
                                    })
                                })  
                        })
                    })

            })
        }); // Call done() after toiet[2] has been created
    });
  });


describe("GET Requests", () => {
    test("Get out all toilets", async () => {
        await db.getAllToilets()
            .then(toilets => {
                expect(toilets).toMatchObject(data.testToilets);
            })
    })

    test("Get toilet reviews for toilet 1", async () => {
        let results: any;

        await db.getToiletReviews(1)
            .then(e => {
                // small workaround
                results = e.map((f) => f);
            })
          
        expect(results).toMatchObject(data.testReviews.filter(e => e.toilet_id == 1));
    })

    test("Get toilet 1", async () => {
        await db.getToilet(1)
            .then(toil => {
                expect(toil[0]).toMatchObject(data.testToilets[0]);
            })
    })

    test("Get toilet without reviews", async () => {
        await db.getToilet(3)
            .then((toilet) => {
                expect(toilet[0]["avg_rating"]).toEqual(0);
            })
    })

    test("Get toilet which doesn't exist", async () => {
        await db.getToilet(4)
            .catch(err => {
                expect(err).toEqual("Not found")
            })
    })

    test("Get all reviews", async () => {
        await db.getAllReviews()
            .then(reviews => {
                expect(reviews).toMatchObject(data.testReviews)
            })
    })

    test("Get review from id: 1", async () => {
        // username is not a row in the database, so it cannot be tested against
        // hard copy
        let current_review = JSON.parse(JSON.stringify(data.testReviews[0]));
        delete current_review["username"];
        await db.getReviewFromId(1)
            .then(reviews => {
                expect(reviews[0]).toMatchObject(current_review);
            })
    })

    test("Get User with id: 1", async () => {
        await db.getUser(data.testUsers[0])
            .then(user => {
                // User type is by default 'user' when created, it has to be updated in the database manually to be set to admin
                expect(user).toMatchObject({"id": 1, "username": 'admin1', 'type': 'user'});
            })
    })

    test("Get user reviews from user with id: 1", async () => {
        await db.getUserReviews(1)
            .then(reviews => {
                expect(reviews).toMatchObject(data.testReviews.map(e => {delete e["username"]; return e}));
            }) 
    })

    
    test("Get all votes for all reviews", async () => {
        await db.getAllReviewVotes()
        .then(votes => {
            expect(votes).toMatchObject(data.testReviewVotes);
        })
    })
    
    test("Get all reviews which User 1 has voted on", async () => {
        await db.getUserVotes(1)
            .then(votes => {
                expect(votes).toMatchObject(data.testReviewVotes.filter(e => e.user_id == 1));
            })
    })

    test("Get all votes for review with id: 1", async () => {
        await db.getReviewVotesFromId(1)
            .then(votes => {
                expect(votes).toMatchObject(data.testReviewVotes.filter(e => e.review_id == 1));
            })
    })

    test("Get a review's rating, from it's id: 1", async () => {
        await db.getReviewRating(1)
            .then(sum => {
                expect(Number(sum[0]["SUM(vote)"])).toEqual(1)
            })
    })

    test("Get a usertoken", async () => {
        await db.login(data.testUsers[0])
            .then(token => {
                // if the db call returns anything, it's a token
                expect(token).not.toEqual(undefined);
            })
    })


})

describe("Edits", () => {
    test("Edit a toilet", async () => {
        let ran = false;
        await db.editToilet(data.testToilets[2], verifiedAdmin)
            .then(() => {
                ran = true;
        })
        
        expect(ran).toBeTruthy;
    })

    test("Edit a review", async () => {
        let ran = false;
        await db.editReview(data.testReviews[0], verifiedAdmin)
            .then(() => {
                ran = true;
        })

        expect(ran).toBeTruthy;
    })
})

describe("Errors", () => {
    test("Create review without being logged in", async () => {
        await db.createReview(data.testReviews[0], {})
            .catch(err => {
                expect(err).toEqual("You need to be logged in to create a review!");
            })
    })

    test("Login with bad password", async () => {
        await db.login({"username": "admin1", "password": "badmin", "type":"user"})
            .catch(err =>{
                expect(err).toEqual("Username or password does not match any known users in the database");
            })
    })

    test("Login with bad user", async () => {
        await db.login({"username": "I don't exist", "password": "nope", "type":"user"})
            .catch(err => {
                expect(err).toEqual("Username or password does not match any known users in the database");
            })
    })

    test("Edit toilet with wrong user", async () => {
        await db.editToilet(data.testToilets[0], {"id": 2, "username": "karl", "password": "karl", "type": "user"})
            .catch(err => {
                expect(err).toEqual("User karl cannot edit this toilet!")
            })
    })

    test("Delete toilet with wrong user", async () => {
        await db.deleteToilet(1, {"id": 2, "username": "karl", "password": "karl", "type": "user"})
            .catch(err => {
                expect(err).toEqual("User karl cannot delete this toilet!")
            })
    })

    test("Edit review with wrong user", async () => {
        await db.editReview(data.testReviews[0], {"id": 2, "username": "karl", "password": "karl", "type": "user"})
            .catch(err => {
                expect(err).toEqual("User karl cannot edit this review!")
            })
    })

    test("Delete review with wrong user", async () => {
        await db.deleteReview(1, {"id": 2, "username": "karl", "password": "karl", "type": "user"})
            .catch(err => {
                expect(err).toEqual(`User karl cannot delete this review!`)
            })
    })

})

describe("Delete requests", () => {
    test("Delete a toilet", async () => {
        let ran = false;
        await db.deleteToilet(1, verifiedAdmin)
            .then(() => {
                ran = true;
            })
            expect(ran).toBeTruthy
    })

    test("Delete a review", async () => {
        let ran = false;
        await db.deleteReview(1, verifiedAdmin)
            .then(() => {
                ran = true;
            })
        expect(ran).toBeTruthy
    })

    test("Remove vote on review", async () => {
        let ran = false;
        await db.removeVote(1, verifiedAdmin)
            .then(() => {
                ran = true;
            })
        expect(ran).toBeTruthy
    })

    test("Remove vote on review as user", async () => {
        let ran = false;
        await db.removeVote(1, data.testUsers[0])
            .then(() => {
                ran = true;
            })
        expect(ran).toBeTruthy
    })


})


/*
async function initalizeDatabase(){
    return new Promise<void>((resolve, reject) => {
        data.testToilets.map(toilet => {
            db.createToilet(toilet, verifiedAdmin);
        })
        data.testReviews.map(review => {
            db.createReview(review, verifiedAdmin);
        })
        data.testReviewVotes.map(review => {
            if (review.review_id) {
                db.voteOnReview(review.review_id, 1, verifiedAdmin);
            }
        })
        data.testUsers.map(user => {
            db.createUser(user);
        })

        resolve();
    })
};

async function clearDatabase(){
    // Start by dropping all the tables
    await db.getAllToilets().then(toilets => {
        toilets.map(toilet => {
            if (toilet.id) {
                db.deleteToilet(toilet.id, verifiedAdmin)
            }
        })
    })

    await db.getAllReviews().then(reviews => {
        reviews.map(review => {
            if (review.id) {
                db.deleteReview(review.id, verifiedAdmin)
            }
        })
    })

    await db.getAllReviewVotes().then(reviewVotes => {
        reviewVotes.map(reviewVote => {
            if (reviewVote.review_id) {
                db.removeVote(reviewVote.review_id, verifiedAdmin)
            }
        })
    })

    await async function() {
        return new Promise<void>((resolve, reject) => {
            pool.query('TRUNCATE TABLE `User`;', (error, _results) => {
                if (error) return reject(error);
                resolve();
            })
        })
    }
}
 */