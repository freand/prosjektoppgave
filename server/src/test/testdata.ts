import { Toilet, Review, User, ReviewVotes } from '../../../types';

export const testToilets: Toilet[] = [
    {
        "id": 1,
        "creator_user_id": 1,
        "name": "testToalettIRealfagsbygget",
        "description": "null",
        "gender": 0,
        "facilities": "ett urinal",
        "poi_id": "718",
        "campus_id": 1,
        "avg_rating": 5.5,
        "calculated_avg": 0,
        "image_link": "https://media.discordapp.net/attachments/284358267579400203/904717722166886450/unknown.png"
    },
    {
        "id": 2,
        "creator_user_id": 1,
        "name": "Dassen på stripa",
        "description": "Dette er en do på stripa",
        "gender": 0,
        "facilities": "doer",
        "poi_id": "73102",
        "campus_id": 1,
        "avg_rating": 6,
        "calculated_avg": 0,
        "image_link": "https://media.discordapp.net/attachments/518855704900141066/907319730203938826/20211108_202324.jpg"
    },
    {
        "id": 3,
        "creator_user_id": 1,
        "name": "Dassen på gløs",
        "description": "Dette er en dårlig do",
        "gender": 0,
        "facilities": "doer",
        "poi_id": "73102",
        "campus_id": 1,
        "avg_rating": 0,
        "calculated_avg": 0,
        "image_link": ""
    }
];

export const testReviews: Review[] = [
    {
        "id": 1,
        "user_id": 1,
        "toilet_id": 1,
        "title": "karl endret mening",
        "description": "doen er dårlig :/",
        "toilet_rating": 6,
        "review_rating": 0,
        "username": "admin1"
    },
    {
        "id": 2,
        "user_id": 1,
        "toilet_id": 2,
        "title": "Doen på stripa lever opp til forventningene!",
        "description": "Tittelen sier alt.",
        "toilet_rating": 6,
        "review_rating": 0,
        "username": "admin1"
    },
    {
        "id": 3,
        "user_id": 1,
        "toilet_id": 1,
        "title": "Meh",
        "description": "null",
        "toilet_rating": 5,
        "review_rating": 0,
        "username": "admin1"
    }
];

export const testReviewVotes: ReviewVotes[] = [
    {
        "user_id": 1,
        "review_id": 1,
        "vote": 1
    },
    {
        "user_id": 1,
        "review_id": 2,
        "vote": 1
    },
    {
        "user_id": 1,
        "review_id": 3,
        "vote": 1
    },
    {
        "user_id": 1,
        "review_id": 4,
        "vote": -1
    },
    {
        "user_id": 1,
        "review_id": 5,
        "vote": 1
    }
];

export const testUsers: User[] = [
    {
        "id": 1,
        "username": "admin1",
        "password": "admin",
        "type": "admin"
    },
    {
        "id": 2,
        "username": "karl",
        "password": "karl",
        "type": "user"
    },
    {
        "id": 3,
        "username": "admin2",
        "password": "admin",
        "type": "admin"
    }
]
export const testUsersWithHashes: User[] = [
    {
        "id": 1,
        "username": "admin1",
        "password": "$2b$10$IBq0YxQy0SRtDB1nQhBEaOlwHfwr8tm/8/CkcBFeSgL9Uh.tjCIs.",
        "type": "admin"
    },
    {
        "id": 2,
        "username": "karl",
        "password": "$2b$10$inZNv3jkIIfqhMqC9hvz5emPIlsgXDIsXBT1bvxih8SiG7NnJpzay",
        "type": "user"
    },
    {
        "id": 3,
        "username": "admin2",
        "password": "$2b$10$GAZeLwEqgdD4zqjhcYPDeurH8okCMWt5rFbphFb/wmAAubsd8FGUm",
        "type": "admin"
    }
];

export const toilet_to_post: Toilet = {
    "id": 0,
    "creator_user_id": 1,
    "name": "posted toil",
    "description": "null",
    "gender": 0,
    "facilities": "nah",
    "poi_id": "1",
    "campus_id": 1,
    "avg_rating": 0,
    "calculated_avg": 0,
    "image_link": "https://media.discordapp.net/attachments/518855704900141066/911273707400466432/unknown.png"
}

export const review_to_post: Review = {
    "id": 5,
    "user_id": 1,
    "toilet_id": 2,
    "title": "Karl liker",
    "description": "denne doen",
    "toilet_rating": 6,
    "review_rating": 0,
    "username": "Karlll"
}

export const testuser_to_post: User = {
    "id": 4,
    "username": "bob",
    "password": "marley",
    "type": "testuser"
}

export const testuser_to_login: User = {
    "id": 2,
    "username": "karl",
    "password": "karl",
    "type": "user"
}

export const adminuser: string = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluMSIsImlkIjoxLCJ0eXBlIjoiYWRtaW4iLCJpYXQiOjE2MzczMzY1NjMsImV4cCI6ODgwMzcyNTAxNjN9.Nbbk66rtE3r3j7xxnNT7FtGzPuze4T0Oqs0uxYYQtSg" // This token expires in 999999D from 19/11/2021