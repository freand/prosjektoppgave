"strict-mode";
import cookieParser from 'cookie-parser'
import dbRouter from "./dbRouter";
import express from "express";
import cors from "cors"

export const app = express();
app.use(cookieParser())
app.use(express.json());
app.use(cors({ credentials: true, origin: "http://localhost:8080"}))
// To make the code easier to read
app.use('/api/v1', dbRouter);

const PORT: number = 3001;
app.listen(PORT, () => {
  console.info(`Server running on port ${PORT}`);
});
