import { Toilet, Review, User, ReviewVotes, Rating, Vote } from '../../types';
import { pool, tempPool } from './mysqlpool';
import { generateToken, verifyToken } from './token';
import { makeHash } from "./hashing";
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';

/**
 * TODO: GET all reviews which user has voted on?
*/

/**
 * database class for all SQL requests
 */
class db{
    /**
     * !!!!!!!!!!!!!!!!!!
     * GET requests
     * !!!!!!!!!!!!!!!!!!
     */

    /**
     * Get all toilets
     * 
     * @params void
     * @returns Promise<Toilet[]>
     */
    getAllToilets(){
        //Get all
        return new Promise<Toilet[]>((resolve, reject) => {
            let newresult: Toilet[] = []
            pool.query('SELECT * FROM Toilet', async (error, results: Toilet[]) => {
                if (error) return reject(error);

                if (results.length < 1) {
                    return reject("Not found")
                } else {
                    const getting = results.map(async (toil, index) => {
                        return new Promise<void>((resolve, reject)=> {
                            if (toil.id) {
                                this.getToiletAverage(toil.id)
                                    .then((e: Rating[]) => {
                                        if (e.length < 1) {
                                            newresult.push(results[index]);
                                            resolve();
                                        } else {
                                            results[index]["avg_rating"] = Number(e[0]['AVG(toilet_rating)']);
                                            newresult.push(results[index]);
                                            resolve();
                                        }
                                        })
                                    .catch(err => reject(err))
                            }
                        })
                        
                    })
                    await Promise.all(getting)
                        .then(() => {
                            resolve(newresult);
                        })
                        .catch(err => reject(err));
                }
            })
        });
    }

    /**
     * Get a toilet, defined by the toilet's id
     * 
     * @param id 
     * @returns Promise<Toilet[]>  // toilet[0] is the toilet where toilet_id == id
     */
    getToilet(id: number){
        // Get one toilet
        return new Promise<Toilet[]>((resolve, reject) => {
            pool.query('SELECT * FROM Toilet WHERE id = ?', [id], (error, results) => {
                if (error) return reject(error);
                if (results.length < 1) {
                    return reject("Not found")
                } else {
                    this.getToiletAverage(id).then((e: Rating[]) => {
                        if (e.length < 1) {
                            resolve(results)
                        } else {
                            results[0]["avg_rating"] = Number(e[0]["AVG(toilet_rating)"]);
                            resolve(results);
                        }
                    })
                }
        
              });
        
        });
    }

    /**
     * Get all reviews for all toilets
     * 
     * @params void 
     * @returns Promise<Review[]>
     */
    getAllReviews(){
        return new Promise<Review[]>((resolve, reject) => {
            pool.query('SELECT Review.*, `User`.`username` FROM Review LEFT JOIN `User` ON Review.user_id = `User`.`id` ', (error, results) => {
                if (error) return reject(error);
                resolve(results);
            })
        });
    }

    /**
     * Get all reviews connected to a toilet, defined by toilet id
     * 
     * @param toiletId 
     * @returns Promise<Review[]> 
     */
     getToiletReviews(toiletId: number){
        return new Promise<Review[]>((resolve, reject) => {
            pool.query('SELECT Review.*, `User`.`username` FROM Review LEFT JOIN `User` ON Review.user_id = `User`.`id` WHERE Review.toilet_id = ?', [toiletId], (error, results) => {
                if (error) return reject(error);

                resolve(results);
            })
        });
    }

    /**
     * Get a review, defined by reviewid
     * 
     * @param reviewId 
     * @returns Promise<Review[]> // review[0] is the review where review_id == id
     */
    getReviewFromId(reviewId: number){
        // Get a review
        return new Promise<Review[]>((resolve, reject) => {
            pool.query('SELECT * FROM Review WHERE id = ?', [reviewId], (error, results) => {
                if (error) return reject(error);

                resolve(results);
            })
        });
    }

    /**
     * Gets all user info from username
     * 
     * @param user 
     * @returns User with hashed password
     */
    getUser(user: jwt.JwtPayload | User){
        return new Promise<User>((resolve, reject) => {
            pool.query('SELECT * from User where username = ?', [user.username], (error, results) => {
                if (error) return reject(error);

                resolve(results[0]);
            })
        });
    }

    /**
     * Get all of a user's reviews
     * 
     * @param userId 
     * @returns Promise<Review[]>
     */
    getUserReviews(userId: number){
        return new Promise<Review[]>((resolve, reject) => {
            pool.query('SELECT * FROM Review WHERE user_id = ?', [userId], (error, results) => {
                if (error) return reject(error);

                resolve(results);
            })
        });
    }

    getUserVotes(userId: number){
        return new Promise<ReviewVotes[]>((resolve, reject) => {
            pool.query('SELECT * FROM ReviewVotes WHERE user_id = ?', [userId], (error, results) => {
                if (error) return reject(error);

                resolve(results);
            })
        })
    }

    getAllReviewVotes(){
        return new Promise<ReviewVotes[]>((resolve, reject) => {
            pool.query('SELECT * FROM ReviewVotes', (error, results) => {
                if (error) return reject(error);
                
                resolve(results);
            } )
        })
    }

    getReviewVotesFromId(reviewId: number){
        return new Promise<ReviewVotes[]>((resolve, reject) => {
            pool.query('SELECT * FROM ReviewVotes WHERE review_id = ?', [reviewId], (error, results) => {
                if (error) return reject(error);

                resolve(results);
            })
        })
    }

    /**
     * Get a toilet's average rating, from reviews
     * 
     * @param toiletId 
     * @returns Promise<number>
     */
    getToiletAverage(toiletId: number){
        return new Promise<Rating[]>((resolve, reject) => {
            pool.query('SELECT AVG(toilet_rating) FROM Review WHERE toilet_id = ? GROUP BY toilet_id', [toiletId], (error, results) => {
                if (error) return reject(error);
                
                resolve(results);
            })
        })
    }

    /**
     * Get a review's rating, based on the sum from all votes
     * 
     * @param reviewId 
     * @returns Promise<Vote[]>
     */
    getReviewRating(reviewId: number){
        return new Promise<Vote[]>((resolve, reject) => {
            pool.query('SELECT SUM(vote) FROM ReviewVotes WHERE review_id = ? GROUP BY review_id', [reviewId], (error, results) => {
                if (error) return reject(error);
                
                resolve(results);
            })
        })
    }

    /**
     * !!!!!!!!!!!!!!!!!!
     * Post requests
     * !!!!!!!!!!!!!!!!!!
     */

    /**
     * Creates a toilet
     * 
     * @param toilet 
     * @param user 
     * @returns <void>
     */
    createToilet(inptoilet: Toilet, user: jwt.JwtPayload){

        // Because of issues with testing we need to do a deep copy (structured cloning) to avoid editing the testdata
        let toilet: Toilet = JSON.parse(JSON.stringify(inptoilet))

        if (!toilet.image_link?.match(/[\/.](gif|jpg|jpeg|tiff|png)$/i)) {
            // invalid link, remove it
            toilet.image_link = "";
        }
        // The user should not be able to set the toilet's id or avg_rating manually
        toilet["creator_user_id"] = user.id;
        delete toilet["id"];
        delete toilet["avg_rating"];
        toilet["calculated_avg"] = 0;
        
        // Get out all toilet attributes dynamically
        let values: Array<number | string | boolean | undefined | null> = []
        let SQLQUERY: string = "INSERT INTO Toilet("
        for (let key in toilet){
            values.push(toilet[key])
            // Create sql query dynamically, to account for changes in  the db
            SQLQUERY += key
            SQLQUERY += ", "
        }
        SQLQUERY = SQLQUERY.slice(0, -2)
        SQLQUERY += ") "
        SQLQUERY += "VALUES("
        for (let key in toilet){
            SQLQUERY += `?, `
        }
        SQLQUERY = SQLQUERY.slice(0, -2)
        SQLQUERY += ")"
        // values = [null, toilet.name, toilet.description, toilet.gender, toilet.facilities, toilet.poi_id, toilet.campus_id, null, toilet.image_link];


        return new Promise<void>((resolve, reject) => {
            pool.query(SQLQUERY, values, (error, results) => {
                if (error) return reject(error);

                resolve();
            })
        })
    }
    
    /**
     * Create a review
     * 
     * @param review 
     * @param user 
     * @returns <void>
     */
    createReview(inpreview: Review, user: jwt.JwtPayload){
        
        // Because of issues with testing we need to do a deep copy (structured cloning) to avoid editing the testdata
        let review: Review = JSON.parse(JSON.stringify(inpreview))

        return new Promise<void>((resolve, reject) => {

            if (user.id){
                // Cleaning the input
                delete review["id"];
                review["user_id"] = user.id;
                review["review_rating"] = 0;
                delete review["username"];
    

                let values: Array<number | string | boolean | undefined | null> = []
                let SQLQUERY: string = "INSERT INTO Review("
                for (let key in review){
                    values.push(review[key])
                    // Create sql query dynamically, to account for changes in  the db
                    SQLQUERY += key
                    SQLQUERY += ", "
                }
                SQLQUERY = SQLQUERY.slice(0, -2)
                SQLQUERY += ") "
                SQLQUERY += "VALUES("
                for (let key in review){
                    SQLQUERY += `?, `
                }
                SQLQUERY = SQLQUERY.slice(0, -2)
                SQLQUERY += ")"
    
                pool.query(SQLQUERY, values, (error, results) => {
                    if (error) return reject(error);
    
                    resolve();
                })
            } else {
                reject("You need to be logged in to create a review!")
            }

        })
    }

    /**
     * Whenever a review is upvoted or downvoted this is called
     * 
     * @param reviewId 
     * @param vote up or down?
     * @returns void
     */
    voteOnReview(reviewId: number, vote: number, user: jwt.JwtPayload | User){
        return new Promise<void>((resolve, reject) => {
            pool.query('INSERT INTO ReviewVotes VALUES(?, ?, ?)', [user.id, reviewId, vote], (error, results) => {
                if (error) return reject(error);

                resolve();
            })
        })
    }

    /**
     * Create a new user
     * 
     * @param user 
     */
     createUser(user: jwt.JwtPayload | User){
        return new Promise<string>((resolve, reject) => {
            makeHash(user.password)
                .then((hash) => {
                    pool.query('INSERT INTO User (username, password) VALUES(?, ?)', [user.username, hash], (error, results) => {
                        if (error) return reject(error);

            
                        resolve("success");
                    })

                })
                .catch((err) => {
                    reject(err)
                })

        })


    }

    /**
     * Log in user, get token
     * 
     * @param user 
     * @returns jwtToken
     */
    login(user: jwt.JwtPayload | User) {
        return new Promise<string>((resolve, reject) => {
            this.getUser(user)
                .then((dbUser: User) => {
                    if (!dbUser){
                        reject("Username or password does not match any known users in the database")
                    } else {
                        // @ts-ignore User.password is not undefined?
                        bcrypt.compare(user.password, dbUser.password)
                            .then((cmp) => {
                                if(cmp){
                                    resolve(generateToken(dbUser));
                                } else {
                                    reject("Username or password does not match any known users in the database")
                                }
                            }
                            ).catch((err) => {
                                reject(err);
                            })
                    }
                })
                .catch((err) => {
                    reject(err)
                })
        
        })
    }

    /**
     * !!!!!!!!!!!!!!!!!!
     * Put requests
     * !!!!!!!!!!!!!!!!!!
     */

    /**
     * Edits a toilet
     * 
     * @param toilet 
     * @param user 
     * @returns <void>
     */
    editToilet(inptoilet: Toilet, user: jwt.JwtPayload | User){

        // Because of issues with testing we need to do a deep copy (structured cloning) to avoid editing the testdata
        let toilet: Toilet = JSON.parse(JSON.stringify(inptoilet))

        return new Promise<void>((resolve, reject) => {
            if (user.id === toilet.creator_user_id || user.type === "admin"){
                if (!toilet.image_link?.match(/[\/.](gif|jpg|jpeg|tiff|png)$/i)) {
                    // invalid link, remove it
                    toilet.image_link = "";
                }
                // The user should not be able to set the toilet's id or avg_rating manually
                let id = toilet.id;
                delete toilet.id;
                delete toilet.avg_rating;
                delete toilet.calculated_avg;
                delete toilet.creator_user_id;
        
                // Get out all toilet attributes dynamically
                let SQLQUERY = "UPDATE Toilet SET "
                let values: Array<number | string | boolean | undefined | null> = []
                for (let key in toilet){
                    // values = [null, toilet.name, toilet.description, toilet.gender, toilet.facilities, toilet.poi_id, toilet.campus_id, null, toilet.image_link];
                    values.push(toilet[key])
                    // Create SQL query dynamically
                    SQLQUERY += `${key}=?, `
                }
                // remove the last comma, to avoid SQL errors
                SQLQUERY = SQLQUERY.slice(0, -2);
                SQLQUERY += ` WHERE id=?`;

                pool.query(SQLQUERY, [...values, id], (error, results) => {
                    if (error) return reject(error);

                    resolve();
                })  
            } else {
                reject(`User ${user.username} cannot edit this toilet!`)
            }
        })

    }

    /**
     * Edit a review
     * 
     * @param {Review}review
     * @param {User}user
     * @returns {void}void
     */
    editReview(inpreview: Review, user: jwt.JwtPayload | User){

        // Because of issues with testing we need to do a deep copy (structured cloning) to avoid editing the testdata
        let review: Review = JSON.parse(JSON.stringify(inpreview))

        return new Promise<void>((resolve, reject) => {
            // @ts-ignore
            this.getReviewFromId(review.id)
                .then((dbReview) => {
                    if (user.id === dbReview[0].user_id || user.type === "admin") {
                        // The user should not be able to change these values
                        delete review["id"];
                        delete review["user_id"];
                        delete review["toilet_id"];
                        delete review["review_rating"];
        
                        // Get out all toilet attributes dynamically
                        let SQLQUERY = "UPDATE Review SET "
                        let values: Array<number | string | boolean | undefined | null> = []
                        for (let key in review){
                            // values = [null, null, null, review.title, review.description, review.toilet_rating, null];
                            values.push(review[key])
                            // Create SQL query dynamically
                            SQLQUERY += `${key}=?, `
                        }
                        // remove the last comma, to avoid SQL errors
                        SQLQUERY = SQLQUERY.slice(0, -2);
                        SQLQUERY += ` WHERE id=?`;
        
                        pool.query(SQLQUERY, [...values, dbReview[0].id], (error, results) => {
                            if (error) return reject(error);
        
                            resolve();
                        })  
                    } else {
                        reject(`User ${user.username} cannot edit this review!`)
                    }
                    })
                .catch((err) => {
                    reject(err)
                })
        })
    }

    /**
     * !!!!!!!!!!!!!!!!!!
     * Delete requests
     * !!!!!!!!!!!!!!!!!!
     */

    /**
     * Delete a toilet
     * Which also deletes reviews?
     * 
     * @param toilet: Toilet
     * @param user: jwt.JwtPayload | User
     */
    deleteToilet(toiletid: number, user: jwt.JwtPayload | User){

        return new Promise<void>((resolve, reject) => {
            this.getToilet(toiletid)
                .then((gottentoilet) => {
                    let toilet: Toilet = gottentoilet[0]

                    if (user.id === toilet.creator_user_id || user.type === "admin"){
                        // Deletes all reviews for a toilet, then the toilet
                        // This can be written as only one query if we set up foreign keys with on delete cascade
                        pool.query('DELETE FROM ReviewVotes WHERE review_id IN (SELECT id FROM Review WHERE toilet_id = ?)', [toilet.id], (error, results) => {
                            if (error) return reject(error);
                            pool.query('DELETE FROM Review WHERE toilet_id = ?', [toilet.id], (error, results) => {
                                if (error) return reject(error);
                                pool.query('DELETE FROM Toilet WHERE id = ?', [toilet.id], (error, results) => {
                                    if (error) return reject(error);
                                    resolve()
                                })
                            })
                        })
                    } else {
                        reject(`User ${user.username} cannot delete this toilet!`)
                    }

                })
                .catch((error) => {
                    reject(error);
                })
            
        })
    }

    /**
     * Delete a review (reviewid)
     * 
     * @param reviewId
     */
    deleteReview(reviewId: number, user: jwt.JwtPayload | User){
        return new Promise<void>((resolve, reject) => {
            this.getReviewFromId(reviewId)
                .then((gottenreview) => {
                    let review: Review = gottenreview[0]

                    if (user.id === review.user_id || user.type === "admin"){
                        pool.query('DELETE FROM ReviewVotes WHERE review_id = ?', [review.id], (error, _) => {
                            if (error) return reject(error);

                            pool.query('DELETE FROM Review WHERE id = ?', [review.id], (error, _) => {
                                if (error) return reject(error);

                                resolve();
                            })
                        })
                    } else {
                        reject(`User ${user.username} cannot delete this review!`)
                    }

                })
                .catch((error) => {
                    reject(error);
                })
        })

    }

    /**
     * Delete a vote on a review
     * 
     * @param reviewId
     * @param User
     * @returns <void>
     */
    removeVote(reviewId: number, user: jwt.JwtPayload | User){
        return new Promise<string>((resolve, reject) => {
            if (user.type == "admin"){
                pool.query('DELETE FROM ReviewVotes WHERE review_id  = ?', [reviewId], (error, results) => {
                    if (error) return reject(error);
    
                    if (results.affectedrows < 1){
                        resolve("no")
                    } else {
                        resolve("success");
                    }
                })
            } else {
                pool.query('DELETE FROM ReviewVotes WHERE review_id  = ? AND user_id = ?', [reviewId, user.id], (error, results) => {
                    if (error) return reject(error);
    
                    if (results.affectedrows < 1){
                        resolve("no")
                    } else {
                        resolve("success");
                    }
                })

            }

        })
    }

    
}


/**
 * A service class for calling the SQL server connected to ToiletMap
 */
let dataService = new db();
export default dataService;

/* for (let i=0; i<15; i++){
    let title = (Math.floor(Math.random() * 5) == 3) ? `Dårlig do.`:`Bruker ${Math.floor(Math.random()*20)} syntes doen var grei`
    let description = "orem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentesque pellentesque massa non rhoncus. Praesent varius ex eu ex viverra ultrices. Donec pretium eros nibh, vitae commodo ex lacinia eget. Pellentesque pretium pulvinar massa ac viverra. Nullam eget enim sed ipsum molestie dapibus ut in elit. Aliquam erat volutpat."
    let toilet_rating = Math.floor(Math.random() * 6);

    let review: Review = {
        "id": 0,
        "user_id":i,
        "toilet_id":1,
        "title": title, 
        "description": description, 
        "toilet_rating": 0,
        "review_rating": 0,
        "username": undefined
    }

    verifyToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluMSIsImlkIjoxLCJ0eXBlIjoiYWRtaW4iLCJpYXQiOjE2Mzc0OTU5MTEsImV4cCI6MTYzNzU4MjMxMX0._9EI9DBxDWrK7QiR9CSnPbvBVHR_SflSZQNKn3AHwtA")
.then(e => {
    dataService.createReview(review,e )})
} */