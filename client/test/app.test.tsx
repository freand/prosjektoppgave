import * as React from 'react';
import { shallow } from 'enzyme';
import Header from '../src/header';
import { Navbar, Container, Nav, Form, FormControl, Button } from 'react-bootstrap';
import { RenderMap } from '../src/mapStuff';
import { NewReview } from '../src/AddReview';
import Search from '../src/search';
import 'regenerator-runtime/runtime'

describe('Header tests', () => {
  test('Header snapshot test', () => {
    const wrapper = shallow(<Header/>);

    expect(wrapper).toMatchSnapshot();
  });
});

describe('AddReview tests', () => {
    test('AddReview test', () => {
        const wrapper = shallow(<NewReview/>);
    
        expect(wrapper).toMatchSnapshot();
    });
});

describe('Search tests', () => {
    test('Search test', () => {
        const wrapper = shallow(<Search searchString=""/>);
    
        expect(wrapper).toMatchSnapshot();
    });
});
